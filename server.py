import xmlrpclib
from SimpleXMLRPCServer import SimpleXMLRPCServer

server = SimpleXMLRPCServer(("192.168.21.57", 8789)) #Crear Server
server.register_introspection_functions()


def dolares(x):
    return int(x) * 19
server.register_function(dolares, 'usd')

def euros(e):
    return int(e)* 20
server.register_function(euros, 'eu')

def yenes(x):
    return int(x) * 6
server.register_function(yenes, 'yen')

def reald(x):
    return float(x) * 0.17
server.register_function(reald, 'real')

server.serve_forever()
